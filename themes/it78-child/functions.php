<?php

require __DIR__ . '/inc/enqueue-scripts.php';
require __DIR__ . '/inc/inner-functions.php';


if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Налаштування сайту',
        'menu_title' => 'Налаштування сайту',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Налаштування шапки',
        'menu_title' => 'Header',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Налаштування подвала',
        'menu_title' => 'Footer',
        'parent_slug' => 'theme-general-settings',
    ));

}

add_filter( 'upload_mimes', 'svg_upload_allow' );
function svg_upload_allow( $mimes ) {
    $mimes['svg']  = 'image/svg+xml';

    return $mimes;
}

add_filter( 'wp_nav_menu_items', 'change_nav_menu_items', 10, 2 );

function change_nav_menu_items( $items, $args ) {

	if ( 'menu-1' == $args->theme_location ) {
		$items .= '<li class="social-icons-mobile">' . get_social() . '</li>';
	}

	return $items;
}

add_action( 'widgets_init', 'register_it78_widgets' );
function register_it78_widgets(){

	register_sidebar( array(
		'name'          => __('Footer menu'),
		'id'            => "footer-menu",
		'description'   => 'Зона для виводу меню в підвалі сайту',
		'class'         => 'footer-menu',
		'before_widget' => '<nav id="%1$s" class="widget %2$s">',
		'after_widget'  => "</nav>\n",
	) );
}