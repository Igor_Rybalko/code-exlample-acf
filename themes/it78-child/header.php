<?php

?>
    <!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'it78' ); ?></a>

    <header id="masthead" class="site-header container">

		<?php
		if ( get_field( 'site_logo', 'options' ) ) :
			$image_ID = get_field( 'site_logo', 'options' )['ID'];
			?>
            <a href="<?php echo get_home_url() ?>" class="logo-link">
                <div class="site-branding"><?php
					echo wp_get_attachment_image( $image_ID, 'full', false, [
						'alt'   => 'Logo_image',
						'class' => 'logo-image'
					] );
					if ( get_field( 'slogan', 'options' ) ) : ?>
                        <h4 class="slogan"><?php echo __( get_field( 'slogan', 'options' ) ) ?></h4>
					<?php
					endif; ?>
                </div><!-- .site-branding -->
            </a>

		<?php
		endif;
		?>

        <nav id="site-navigation" class="main-navigation">
            <div class="wrapper">
                <div class="menu-toggle" aria-controls="primary-menu"
                     aria-expanded="false"><img
                            src="<?php echo get_stylesheet_directory_uri() . '/assets/images/burger.svg' ?>"
                            alt="menu-toggle"></div>

				<?php

				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);

				if ( get_field( 'social', 'options' ) ) :
					echo get_social();
				endif;
				?>
            </div>
        </nav><!-- #site-navigation -->

    </header><!-- #masthead -->
<?php