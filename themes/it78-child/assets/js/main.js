jQuery(document).ready(function ($) {
    AOS.init({disable: 'mobile'});

    if ($('.menu-toggle')) {
        $('.menu-toggle').on('click', function () {
            $('.menu-menu-1-container').toggleClass('active');
        });
    }


    const maskList = [
        {"code": "+380 ## ### ## ##"},
    ];
    const mask = (selector) => {
        function setMask() {
            let matrix = '+380############';
            maskList.forEach(item => {
                let code = item.code.replace(/[\s#]/g, ''),
                    phone = this.value.replace(/[\s#-)(]/g, '');

                if (phone.includes(code)) {
                    console.log(phone, code);
                    matrix = item.code;
                }
            });

            let i = 0,
                val = this.value.replace(/\D/g, '');

            this.value = matrix.replace(/(?!\+)./g, function (a) {
                return /[#\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? '' : a;
            });
        }

        let inputs = document.querySelectorAll(selector);

        inputs.forEach(input => {
            input.onfocus = function () {
                this.value = '+380'
            };
            input.addEventListener('input', setMask);
            input.addEventListener('focus', setMask);
            input.addEventListener('blur', setMask);
        });
    };
    if ($('#form_tel')) {
        mask("#form_tel");
    }

    if ($('.form-request')) {
        $('.form-request').on('click', function (event) {
            event.stopPropagation();
            $('.form-popUp').addClass('active');
        });
    }

    if ($('.close-button')) {
        $('.close-button').on('click', function (event) {
            event.stopPropagation();
            $('.form-popUp').removeClass('active');
        });
    }

    if ($('.wpcf7-form')) {
        $('.wpcf7-form').on('submit', function () {
            setTimeout(function () {
                $('.form-popUp').removeClass('active');
            }, 2000);

        });
    }

    if ($('.advantages-wrapper')) {
        slickInit();
        $(window).on('resize', function () {
            slickInit();
        });
    }

    function slickInit() {
        let windowWidth = $(window).width();
        if (windowWidth <= 640) {
            $('.advantages-wrapper').slick(
                {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true
                }
            );
        } else if (windowWidth > 640 ) {
            $('.advantages-wrapper').slick('unslick');
        }
    }

});