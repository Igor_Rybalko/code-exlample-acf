<?php
$title        = $args['title'];
$image_before = $args['image_before'];
$image_after  = $args['image_after'];
$services     = get_field( 'services', 'options' );
if ( isset( $services ) && $services ) : ?>

    <section class="services">
        <div class="container">
            <div class="services-content">
				<?php
				if ( isset( $image_before ) && $image_before ):
					echo wp_get_attachment_image( $image_before, 'full', false, [
						'alt'      => 'image',
						'class'    => 'services-image',
						'data-aos' => 'zoom-in-right',
						'data-aos-duration' => 1000
					] );
				endif;
				?>
                <div class="services-wrapper">

					<?php if ( isset( $title ) && $title ) :
						echo "<h2 class='services-title'>" . $title . "</h2>";
					endif; ?>
                    <div class="wrapper">
                        <ul class="services-items">
							<?php foreach ( $services as $service ) : ?>
                                <li class="service"><?php echo $service['service']; ?></li>
							<?php
							endforeach; ?>
                        </ul>
						<?php
						if ( isset( $image_after ) && $image_after ):
							echo wp_get_attachment_image( $image_after, 'full', false, [
								'alt'   => 'image',
								'class' => 'services-image mobile',

							] );
						endif;
						?>
                    </div>

                </div>

				<?php
				if ( isset( $image_after ) && $image_after ):
					echo wp_get_attachment_image( $image_after, 'full', false, [
						'alt'               => 'image',
						'class'             => 'services-image',
						'data-aos'          => 'zoom-in-left',
						'data-aos-duration' => 1000
					] );
				endif;
				?>
            </div>
        </div>

    </section>
<?php
endif;