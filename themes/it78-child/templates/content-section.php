<?php
$content = ( $args['content'] ) ? $args['content'] : null;
if ( isset( $content ) && $content ) : ?>
    <section class="about">
        <div class="container">
			<?php echo __( $content, 'it78' ) ?>
        </div>
        <div style="clear: both"></div>
    </section>
<?php
endif;