<?php
$section_title = $args['section_title'];
$sub_title     = $args['sub_title'];
$advantages    = $args['advantages'];
if ( isset( $advantages ) && $advantages ) : ?>

    <section class="advantages">
        <div class="container">
			<?php if ( isset( $section_title ) && $section_title ) : ?>
                <h2 class="advantages-title" data-aos="zoom-in" data-aos-duration="1500"><?php echo __( $section_title, 'it78' ) ?></h2>
			<?php endif; ?>
			<?php if ( isset( $sub_title ) && $sub_title ) : ?>
                <h3 class="advantages-sub_title"><?php echo __( $sub_title, 'it78' ) ?></h3>
			<?php endif; ?>
            <div class="advantages-wrapper">
				<?php
                $state = true;
                foreach ( $advantages as $index => $advantage ) :
                    $data_attr = ($state) ? 'data-aos="zoom-in-left"' : 'data-aos="zoom-in-right"'?>
                    <div class="advantages-wrapper-item" <?php echo $data_attr ?>>
                        <div class="wrapper">
                            <p class="description">it78</p>
                            <div class="content"><?php echo __( $advantage['property'], 'it78' ); ?></div>
                        </div>
                        <div class="counter">
                            <?php echo $index+1 ?>
                        </div>
                    </div>
				<?php
                $state = !$state;
				endforeach; ?>
            </div>
        </div>
    </section>

<?php
endif;
