<?php
if ( have_rows( 'constructor' ) ):

	// Loop through rows.
	while ( have_rows( 'constructor' ) ) : the_row();

		// Case: Paragraph layout.
		if ( get_row_layout() == 'services_section' ) {

			$section      = get_sub_field( 'services_section' );
			$title        = ( $section['section_title'] ) ? $section['section_title'] : null;
			$image_before = ( $section['image_before'] ) ? $section['image_before'] : null;
			$image_after  = ( $section['image_after'] ) ? $section['image_after'] : null;
			get_template_part( 'templates/services', 'section', [
				'title'        => $title,
				'image_before' => $image_before,
				'image_after'  => $image_after
			] );
		} elseif ( get_row_layout() == 'banner_section' ) {
			$main_image_id      = ( get_sub_field( 'desktop_image' ) ) ? get_sub_field( 'desktop_image' ) : null;
			$secondary_image_id = ( get_sub_field( 'mobile_image' ) ) ? get_sub_field( 'mobile_image' ) : null;
			get_template_part( 'templates/banner', 'section', [
				'main_image_id'      => $main_image_id,
				'secondary_image_id' => $secondary_image_id
			] );
		} elseif ( get_row_layout() == 'statistic_section' ) {
			{
				{

					$stat = [];

					if ( have_rows( 'option' ) ):

						while ( have_rows( 'option' ) ) : the_row();

							$stat[] = get_sub_field( 'option_group' );

						endwhile;

					endif;
					get_template_part( 'templates/statistic', 'section', [ 'statistic' => $stat ] );
				}
			}
		} elseif ( get_row_layout() == 'propertyes_section' ) {
			$section_title = ( get_sub_field( 'section_title' ) ) ? get_sub_field( 'section_title' ) : null;
			$sub_title     = ( get_sub_field( 'section_sub_title' ) ) ? get_sub_field( 'section_sub_title' ) : null;
			$advantages    = ( get_sub_field( 'advantages' ) ) ? get_sub_field( 'advantages' ) : null;
			get_template_part( 'templates/advantages', 'section', [
				'section_title' => $section_title,
				'sub_title'     => $sub_title,
				'advantages'    => $advantages
			] );
		} elseif ( get_row_layout() == 'timeline_section' ) {
			$section_title  = ( get_sub_field( 'section_title' ) ) ? get_sub_field( 'section_title' ) : null;
			$image_id       = ( get_sub_field( 'banner' ) ) ? get_sub_field( 'banner' ) : null;
			$timeline_group = ( get_sub_field( 'timeline_group' ) ) ? get_sub_field( 'timeline_group' ) : null;
			get_template_part( 'templates/timeline', 'section', [
				'section_title'  => $section_title,
				'image_id'       => $image_id,
				'timeline_group' => $timeline_group
			] );
		} elseif ( get_row_layout() == 'aboutus_section' ) {
			$content = ( get_sub_field( 'content' ) ) ? get_sub_field( 'content' ) : null;
			get_template_part( 'templates/content', 'section', [
				'content' => $content
			] );
		} elseif ( get_row_layout() == 'works_section' ) {
			$works = [];
			if ( have_rows( 'works' ) ):

				while ( have_rows( 'works' ) ) : the_row();
					$work            = new stdClass();
					$work->name    = ( get_sub_field( 'work_name' ) ) ? get_sub_field( 'work_name' ) : null;
					$work->link    = ( get_sub_field( 'work_link' ) ) ? get_sub_field( 'work_link' ) : null;
					$work->gallery = ( get_sub_field( 'work_images' ) ) ? get_sub_field( 'work_images' ) : null;
					$works[]=$work;
				endwhile;

			endif;
			get_template_part( 'templates/works', 'section', [
				'works' => $works
			] );
		}


		// End loop.
	endwhile;

// No value.
else : echo '
<div class="container">
<h2>' . __( "Контент сторінки відсутній!" ) . '</h2>
</div>';
endif; ?>

