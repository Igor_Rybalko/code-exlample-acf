<?php

$works = ( isset( $args['works'] ) && count( $args['works'] ) > 0 ) ? $args['works'] : null;
if ( isset( $works ) ): ?>
    <section class="works">
        <div class="container">
            <div class="works-wrapper">
				<?php foreach ( $works as $work ) : ?>
                    <div class="work" data-aos="zoom-out-up"
                         data-aos-duration="2000">
						<?php
						$name    = ( isset( $work->name ) && $work->name ) ? $work->name : null;
						$link    = ( isset( $work->link ) && $work->link ) ? $work->link : null;
						$gallery = ( isset( $work->gallery ) && $work->gallery ) ? $work->gallery : null;

						if ( $name ):?>
                            <h2 class="work-title"><?php echo __( $name, 'it78' ); ?></h2>
						<?php
						endif;

						if ( $link ):?>
                            <h2 class="work-link"><?php echo $link; ?></h2>
						<?php
						endif;

						if ( $gallery && count( $gallery ) > 0 ):?>
                            <div class="work-gallery">
								<?php
								foreach ( $gallery as $image ) :
									echo wp_get_attachment_image( $image['ID'], 'full', false, [
										'alt'   => 'gallery-image',
										'class' => 'work-gallery-image'
									] );
								endforeach;
								?>
                            </div>
						<?php
						endif;
						?>
                    </div>
				<?php
				endforeach; ?>
            </div>
        </div>
    </section>
<?php
endif;

//echo '<pre>';
//print_r( $works[0]->gallery );
//echo '</pre>';
