<?php
$main_image_id      = $args['main_image_id'];
$secondary_image_id = $args['secondary_image_id'];
if ( isset( $main_image_id ) && $main_image_id ) : ?>
    <section class="banner-image desktop">
        <div class="desktop"><?php echo wp_get_attachment_image( $main_image_id, 'full', false, [
				'alt'               => 'banner-image',
				'data-aos'          => 'zoom-in',
				'data-aos-duration' => 1500
			] ) ?></div>
        <div class="mobile"><?php echo wp_get_attachment_image( $secondary_image_id, 'full', false, [
				'alt'               => 'banner-image',
				'data-aos'          => 'zoom-in',
				'data-aos-duration' => 1500
			] ) ?></div>
    </section>
<?php
endif;
?>

