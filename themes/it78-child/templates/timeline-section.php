<?php
$section_title  = ( $args['section_title'] ) ? $args['section_title'] : null;
$image_id       = ( $args['image_id'] ) ? $args['image_id'] : null;
$timeline_group = ( $args['timeline_group'] ) ? $args['timeline_group'] : null;
if ( isset( $timeline_group ) && count( $timeline_group ) > 0 ) : ?>
    <section class="timeline">
		<?php if ( isset( $section_title ) && $section_title ) : ?>
            <h2 class="timeline-title" data-aos="zoom-in" data-aos-duration="1500">
				<?php echo __( $section_title, 'it-78' ) ?>
            </h2>
		<?php
		endif;
		if ( isset( $image_id ) && $image_id ) :
			echo wp_get_attachment_image( $image_id, 'full', false,
				[
					'alt'               => 'banner-image',
					'class'             => 'timeline-banner',
					'data-aos'          => 'zoom-in',
					'data-aos-duration' => '1500'
				] );
		endif; ?>
        <div class="container">

            <div class="timeline-blocks">
				<?php
				foreach ( $timeline_group as $group ) : ?>
                    <h3 class="block-title"  data-aos="fade-up" data-aos-duration="1500"><?php echo __( $group['block_title'], 'it78' ) ?></h3>
                    <div class="block-content"  data-aos="fade-up" data-aos-duration="1500"><?php echo __( $group['block_content'], 'it78' ) ?></div>
				<?php
				endforeach;
				?>
            </div>

        </div>
    </section>
<?php
endif;

