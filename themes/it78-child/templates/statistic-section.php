<?php
$statistic = $args['statistic'];
if ( isset( $statistic ) && $statistic ) : ?>
    <section class="statistic">
        <div class="container">
            <div class="statistic-wrapper">
				<?php
				foreach ( $statistic as $stat ) : ?>
                    <div class="statistic-item" data-aos="zoom-in" data-aos-duration="1500">
						<?php if ( isset( $stat['option_name'] ) && isset( $stat['option_number'] ) ) : ?>
                            <p class="statistic-item-number"><?php echo $stat['option_number'] ?></p>
                            <p class="statistic-item-name"><?php echo __( $stat['option_name'], 'it78' ) ?></p>
						<?php
						endif; ?>
                    </div>
				<?php
				endforeach; ?>
            </div>
        </div>
    </section>
<?php
endif; ?>
