<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package IT76
 */

?>

<footer id="colophon" class="site-footer">
    <div class="container">
        <div class="columns">
            <div class="column">
			    <?php
			    $logo = ( get_field( 'site_logo', 'options' ) ) ? get_field( 'site_logo', 'options' ) : null;
			    if ( isset( $logo ) && $logo ) :
				    $image_ID = $logo['ID'];
				    echo wp_get_attachment_image( $image_ID, 'full', false, [
					    'alt'   => 'Logo_image',
					    'class' => 'logo-image'
				    ] );
			    endif;

			    $tel = ( get_field( 'tel_group', 'options' ) ) ? get_field( 'tel_group', 'options' ) : null;
			    if ( isset( $tel['tel'] ) && $tel['tel'] != '+'  && $tel['tel']) :
				    ?>
                    <div class="row">
                        <h3 class="title"><?php echo __( 'Телефон', 'it78' ) ?></h3>
                        <a href="tel:<?php echo preg_replace( "/\s+/", "", $tel['tel'] ) ?>"><?php echo preg_replace( "/\s+/", "-", $tel['tel'] ) ?></a>
                    </div>
			    <?php
			    endif;

			    $email = ( get_field( 'email_group', 'options' ) ) ? get_field( 'email_group', 'options' ) : null;
			    if ( isset( $email['email'] ) && $email['email']) : ?>
                    <div class="row">
                        <h3 class="title"><?php echo __( 'Email', 'it78' ) ?></h3>
                        <a href="mailto:<?php echo $email['email'] ?>"><?php echo $email['email'] ?></a>
                    </div>
			    <?php
			    endif;
			    ?>
                <button class="form-request">
				    <?php echo __( 'Замовити консультацію', 'it78' ) ?>
                </button>
            </div>
		    <?php
		    if ( have_rows( 'services', 'options' ) ): ?>
                <div class="column">
                    <div class="row">
                        <h3 class="title"><?php echo __( 'Послуги', 'it78' ) ?></h3>
                        <ul class="services">
						    <?php
						    while ( have_rows( 'services', 'options' ) ) : the_row();
							    $service = get_sub_field( 'service' ); ?>
                                <li class="services-item">
								    <?php echo __( $service, 'it78' ) ?>
                                </li>
						    <?php
						    endwhile; ?>
                        </ul>
                    </div>
                </div>
		    <?php endif; ?>
            <div class="column">
                <div class="row">
                    <h3 class="title"><?php echo __( 'Інформація', 'it78' ) ?></h3>
				    <?php dynamic_sidebar( 'footer-menu' ); ?>
                </div>
            </div>

		    <?php
		    $address = ( get_field( 'address_group', 'options' ) ) ? get_field( 'address_group', 'options' ) : null;
		    if ( isset( $address['address'] ) && $address['address'] ) : ?>
                <div class="column">
                    <div class="row">
                        <h3 class="title"><?php echo __( 'Адреса', 'it78' ) ?></h3>
                        <p><?php echo $address['address'] ?></p>
                    </div>
                </div>
		    <?php
		    endif;
		    ?>

        </div>
    </div>

	<?php
	$reserved = get_field( 'rights_reserved', 'option' );
	if ( isset( $reserved ) && $reserved ) :?>
        <div class="site-info">
            <div class="container">
                <p><?php echo __( get_field( 'rights_reserved', 'option' ), 'it78' ) ?></p>
            </div>
        </div><!-- .site-info -->
	<?php
	endif;
	?>
</footer><!-- #colophon -->
</div><!-- #page -->
<div class="form-popUp">
    <div class="container">
        <div class="wrapper">
            <div class="form-box">
                <div class="close-button">

                </div>
			    <?php echo do_shortcode('[contact-form-7 id="923bbec" title="Contact form"]') ?>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>

</body>
</html>
