let preprocessor = 'sass';
const { src, dest, parallel, series, watch } = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');
function browsersync() {
    browserSync.init({
        server: { baseDir: 'npm i gulp-concat gulp-uglify-es --save-dev' },
        notify: false,
        online: true
    })
}

function scripts() {
    return src([
        'node_modules/jquery/dist/jquery.min.js',
        'assets/js/main.js',
    ])
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(dest('assets/js/'))
        .pipe(browserSync.stream())
}

function startwatch() {

  //  watch(['assets/js/**/*.js', '!assets/js/**/*.min.js', 'assets/scss/**/*.js'], scripts);
    watch('assets/scss/**/*', styles);
}

function styles() {
    return src('assets/scss/style.scss') // Выбираем источник: "app/sass/main.sass" или "app/less/main.less"
        .pipe(eval(preprocessor)()) // Преобразуем значение переменной "preprocessor" в функцию
        .pipe(concat('style.min.css')) // Конкатенируем в файл app.min.js
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true })) // Создадим префиксы с помощью Autoprefixer
        .pipe(cleancss( { level: { 1: { specialComments: 0 } }/* , format: 'beautify' */ } )) // Минифицируем стили
        .pipe(dest('assets/css/')) // Выгрузим результат в папку "app/css/"
}

exports.styles = styles;
exports.browsersync = browsersync;
exports.scripts = scripts;
exports.watch = parallel(styles , startwatch);