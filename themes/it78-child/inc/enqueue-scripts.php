<?php
add_action('wp_enqueue_scripts', 'it78_scripts');
function it78_scripts()
{
    wp_enqueue_style('main-style', get_stylesheet_directory_uri() . '/assets/css/style.min.css');
	wp_enqueue_style('aos-style', 'https://unpkg.com/aos@2.3.1/dist/aos.css');
	wp_enqueue_script('slick-script', get_stylesheet_directory_uri() . '/assets/js/slick.min.js', ['jquery'] ,'1.8.1', true);
	wp_enqueue_script('aos-script', 'https://unpkg.com/aos@2.3.1/dist/aos.js', ['jquery'] ,'', false);
    wp_enqueue_script('main-script', get_stylesheet_directory_uri() . '/assets/js/main.js', ['jquery','slick-script','aos-script'] ,'', true);
}

add_action('admin_enqueue_scripts', 'it78_admin_scripts', 99);
function it78_admin_scripts()
{
	wp_enqueue_script('admin-script', get_stylesheet_directory_uri() . '/assets/js/admin-scripts.js', array('jquery'), '1.0', true);
}