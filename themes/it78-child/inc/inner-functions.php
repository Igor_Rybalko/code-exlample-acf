<?php

function get_social() {
	ob_start(); ?>
    <div class="social-icons">
		<?php
		$icons    = get_field( 'social', 'options' );
		foreach ( $icons as $icon ) :
			$image_id = $icon['icon']['ID'];
			$link = $icon['link']; ?>
            <a href="<?php echo $link ?>" target="_blank">
				<?php echo wp_get_attachment_image( $image_id, 'full', false, [
					'alt'    => 'social-icon',
					'class'  => 'icon-image',
					'width'  => 40,
					'height' => 40,
				] ) ?>
            </a>
		<?php
		endforeach;
		?>
    </div>
	<?php
	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}