<?php
get_header(); ?>
    <style>
        .contacts {
            background-image: url("<?php echo get_field('bg_Image') ?>");
        }
    </style>
    <main id="primary" class="site-main">
        <div class="contacts">
            <div class="container">
                <div class="wrapper">
                    <div class="contacts-info">
						<?php
						$title = ( get_field( 'title' ) ) ? get_field( 'title' ) : null;
						if ( isset( $title ) ) : ?>
                            <h3 class="contacts-info-title">
								<?php echo __( $title, 'it78' ); ?>
                            </h3>
						<?php
						endif;
						$tel     = ( get_field( 'tel_group', 'options' ) ) ? get_field( 'tel_group', 'options' ) : null;
						$email   = ( get_field( 'email_group', 'options' ) ) ? get_field( 'email_group', 'options' ) : null;
						$address = ( get_field( 'address_group', 'options' ) ) ? get_field( 'address_group', 'options' ) : null;
						if ( isset( $tel['tel'] ) || isset( $email['email'] ) || isset( $address['address'] ) ) : ?>
                            <div class="contacts-info-details">

                                <div class="row">
									<?php if ( isset( $tel['icon'] ) && $tel['icon'] ) :
										echo wp_get_attachment_image( $tel['icon'], 'full', false, [
											'alr'   => 'tel_icon',
											'class' => 'icon'
										] );
									endif;
									if ( isset( $tel['tel'] ) && $tel['tel'] ) : ?>
                                        <a href="tel:<?php echo preg_replace( "/\s+/", "", $tel['tel'] ) ?>"><?php echo preg_replace( "/\s+/", "-", $tel['tel'] ) ?></a>
									<?php
									endif; ?>
                                </div>

                                <div class="row">
									<?php if ( isset( $email['email_icon'] ) && $email['email_icon'] ) :
										echo wp_get_attachment_image( $email['email_icon'], 'full', false, [
											'alr'   => 'tel_icon',
											'class' => 'icon'
										] );
									endif;
									if ( isset( $email['email'] ) && $email['email'] ) : ?>
                                        <a href="mailto:<?php echo $email['email'] ?>"><?php echo $email['email'] ?></a>
									<?php
									endif; ?>
                                </div>

                                <div class="row">
									<?php if ( isset( $address['icon'] ) && $address['icon'] ) :
										echo wp_get_attachment_image( $address['icon'], 'full', false, [
											'alr'   => 'tel_icon',
											'class' => 'icon'
										] );
									endif;
									if ( isset( $address['address'] ) && $address['address'] ) : ?>
                                        <a href="https://www.google.com.ua/maps/place/<?php echo $address['address']; ?>" target="_blank"><?php echo $address['address']; ?></a>
									<?php
									endif; ?>
                                </div>

                            </div>
						<?php
						endif; ?>
                    </div>
                    <div class="contacts-form">
                        <?php echo do_shortcode('[contact-form-7 id="923bbec" title="Contact form"]') ?>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- #main -->
<?php
get_footer( 'contacts' );