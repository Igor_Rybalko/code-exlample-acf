<?php
get_header();
?>

    <main id="primary" class="site-main">
		<?php
		$page_id = get_the_ID();
		if ( get_field( 'poster', $page_id ) && get_field( 'poster_video', $page_id ) ): ?>
            <div class="video-wrapper">
                <video autoplay muted loop>
                    <source src="<?php echo get_field( 'poster_video', $page_id ) ?>" type="video/mp4"/>
                </video>
            </div>
		<?php
		endif;

        get_template_part('templates/constructor');
		?>

    </main><!-- #main -->

<?php
get_footer();